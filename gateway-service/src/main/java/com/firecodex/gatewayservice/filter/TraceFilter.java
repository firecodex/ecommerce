package com.firecodex.gatewayservice.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
@Order(0)
public class TraceFilter implements GlobalFilter{

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		System.out.println("Trace ID is generated!");
		exchange.getAttributes().put("X-TRACE-ID", generateTraceId());
		return chain.filter(exchange);
	}
	
	private String generateTraceId() {
		return String.valueOf(System.currentTimeMillis());
	}

}
