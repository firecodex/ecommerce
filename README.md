# Eccomerce System

## Modules & Ports:
1. config-service - 8000
2. discovery-service - 8001
3. auth-service - 8669
4. gateway-service - 8080
5. console-service - 8008
6. inventory-service - 9001
7. search-service - 9000
8. checkout-service - 9002
9. order-service - 9003
10. logistic-service - 9004
11. trace-service - 9007
12. metrics-service - 9009