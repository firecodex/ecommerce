# Discovery Server

It's a centralized real-time discovery server.

# Getting Started

### Build

> mvn spring-boot:run

### Endpoint

* [Centralized Configuration](https://admin:freeaccess@localhost:8001)

