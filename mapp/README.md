# STORE2DOOR
A generic Real Estate  for Property Dealing.

This is an ionic project for property Dealing application. You need to have Cordova and Ionic 4.0.0 installed on the 
system to run it successfully

## Using this project

You must have cordova installed prior to this.

```
    $ npm install -g cordova@8.0.0
```


```
    $ npm install -g ionic@4.12.0
```

NOTE: Don’t install the latest ionic. Latest version of Ionic is totally different from 4.0.0


## Installation of this project

* Extract the zip file you received after purchase

* Install npm dependecies

```
    $ npm install
```

* Install Resources

```
    $ ionic cordova resources
```

* Add Platform (whichever required)

```
    $ ionic cordova platform add android --save
    $ ionic cordova platform add ios --save
```
in few cases, you might need to install the latest platform
```
    $ ionic cordova platform add android@latest
    $ ionic cordova platform add ios@latest
```

* Install Plugins (whichever required)

```
    $ ionic cordova plugin add YOUR_PLUGIN_NAME
```

* Initialize the new git
    ```git init```

* Setup the new git remotes accordingly
    ```git remote add origin new remote```


## Plugins List

```
    "cordova-plugin-geolocation",
    "cordova-plugin-nativegeocoder",
    "cordova-plugin-x-socialsharing",
    "cordova-plugin-whitelist",
    "cordova-plugin-statusbar",
    "cordova-plugin-device",
    "cordova-plugin-splashscreen",
    "cordova-plugin-ionic-webview" ,
    "cordova-plugin-ionic-keyboard"
```


* Run app on device

```
    $ ionic cordova run android
    $ ionic cordova run ios --device
```

* Create signing key for android to release on Google Play

```
    $ keytool -genkey -v -keystore keystore folder address -alias app alias -keyalg RSA -keysize 2048 -validity 10000
```

* Create release build for Android Play Store

```
    $ ionic cordova build android --release
```

* Sign the ‘unsigned’ APK for upload on Play store

```
    $ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .keystore file full path unsigned apk full path app alias
```


* Zipalign to optimize size for play store upload

```
    $ ./zipalign -v 4 signed apk full path path for final APK
``` 
---------------------------------------------------------------------------------
* Contact
If you have any questions, contact us at souravkalra1990@gmail.com or call at +44 743 857 9962.

* Internet Help
Ionic:

   ionic (Ionic CLI)  : 4.0.1 (/usr/local/lib/node_modules/ionic)
   Ionic Framework    : ionic-angular 3.9.2
   @ionic/app-scripts : 3.1.10

Cordova:

   cordova (Cordova CLI) : 8.0.0
   Cordova Platforms     : android 7.1.1, ios 4.5.5

System:

   Android SDK Tools : 26.1.1
   ios-deploy        : 2.0.0
   ios-sim           : 7.0.0
   NodeJS            : v8.11.3 (/usr/local/bin/node)
   npm               : 6.2.0
   OS                : macOS High Sierra
   Xcode             : Xcode 9.4.1 Build version 9F2000

Environment:

   ANDROID_HOME : /Users/eduardoroth/Library/Android/sdk/

You have been opted out of telemetry. To change this, run: cordova telemetry on.

* Final Fixes & Steps

> Change NativeGeocoderReverseResult to NativeGeocoderResult in location.page.ts
> - https://stackoverflow.com/questions/55652890/module-has-no-exported-member-nativegeocoderreverseresult-in-ionic-3-geo

```
    $ npm install -g cordova@8.0.0
    $ npm install -g ionic@4.12.0
    $ npm install
    $ ionic cordova resources - not required
    $ ionic cordova platform add android@latest
    $ ionic cordova platform add ios@latest
    $ ionic cordova run android
    $ ionic cordova run ios --device
```