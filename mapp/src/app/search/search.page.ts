/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  public RecentHistory:Array<any>;
  constructor(
    private modalController: ModalController,
  ) {
    this.RecentHistory = [{productName:'Apple',category:'Fruits'},{productName:'Apple',category:'Fruits'},{productName:'Apple',category:'Fruits'}]
   }

  ngOnInit() {
  }

  dismiss(){
    this.modalController.dismiss();
  }
  clearHistory(){
    console.log('Clear History');
    this.RecentHistory = [];
  }

}
