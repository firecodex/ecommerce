import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, ToastController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  addNewPayment = false
  cardDetails:any;

  
  constructor(private menuCtrl: MenuController,
    public dataService: DataService, 
    private alertController: AlertController,
    private route:Router,
    private toastCtrl:ToastController,
    public cart:CartService,
    public product:ProductsService) {
      this.cart.grandTotal;
      this.cardDetails ={cardNumber:null,cardType:null,cardCvv:null,cardDate:null,zipCode:null}
     }

  ngOnInit() {
  }

  addPayment(){
    this.addNewPayment = !this.addNewPayment;
  }

  async done(){
    const toast = await this.toastCtrl.create({
      message: 'Your order Successfully Complete',
      duration: 2000,
      position:'top'
    });
    toast.present();
    console.log('this.cart.addCart', this.cart.addCart)
    this.product.cartArray = this.cart.addCart
    this.cart.addCart.map((ele) => {
      ele.units =0;
    })
    this.cart.addCart = [];
    this.cart.productQty = 0;
    this.cart.subTotal =0;
    this.cart.grandTotal = 0;
    this.route.navigate(['home'])
  }



  async back() {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Do you want to cancel entering your payment info?',
      buttons: [
        {
          text: 'Yes',
          cssClass: 'mycolor',
          handler: (blah) => {
          console.log('back')
          }
        }, {
          text: 'No',
          role: 'cancel',
          cssClass: 'mycolor',
          handler: () => {}
        }
      ]
    });

    await alert.present();
  }

  SaveCard(){
    if(this.cardDetails.cardType =='visa'){
      this.dataService.current_user.billing.push({card_number:'3124',expiry_date:'12/22',image:'assets/imgs/visa.png'})
    }
    if(this.cardDetails.cardType == 'master'){
      this.dataService.current_user.billing.push({card_number:'3124',expiry_date:'12/22',image:'assets/imgs/mastercard.png'})
    }
    
  }

}
