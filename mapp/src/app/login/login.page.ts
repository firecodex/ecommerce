
/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
public selectedPhoneCode;
public phoneCodeList;
public phoneNumber;
public customPopoverOptions: any = {
}; 

  constructor(
    private http:HttpClient,
    private route:Router
    ) { 
    let phoneCode = this.http.get('assets/phoneCode.json').pipe(map((res:any) => res.phoneList));
    phoneCode.subscribe((res) => {
      this.phoneCodeList  = res
      console.log('phoneCodeList',this.phoneCodeList)
    })
    
   }

  ngOnInit() {
  }

  gotVerification(){
    console.log('phone Number',this.phoneNumber)
    if(this.phoneNumber && this.phoneNumber != ''){
      this.route.navigate(['otp',{ phoneNumber:this.phoneNumber, selectedPhoneCode:this.selectedPhoneCode }]);
    }
    
  }

  skip(){
    this.route.navigate(['home'])
  }
  
}
