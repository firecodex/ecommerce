/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { CartService } from '../services/cart.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {


  constructor(
    public dataServ: DataService,
    public cart: CartService,
    public route: Router,
    public alertController: AlertController
  ) {
    this.addCart();

  }

  addCart() {
    this.cart.addCart.map(el => {
      let total = el.units * el.price;
      this.cart.subTotal = this.cart.subTotal + total;
      this.cart.grandTotal = this.cart.subTotal + this.cart.deliveryCharge;
    });
  }

  ngOnInit() {
    this.addCart();
  }

  startShopping() {
    this.route.navigate(['home'])
  }

  updateCart(productID, type) {
    let productunits = this.cart.addCart.find(el => el.id === productID);
    let id;

    if (type == 'add') {
      productunits.units += 1;
      this.cart.productQty += 1
      let total = 1 * productunits.price
      this.cart.subTotal = this.cart.subTotal + total;
      this.cart.grandTotal = this.cart.subTotal + this.cart.deliveryCharge;
    }

    if (type == 'remove') {

      if (productunits.units > 1) {
        productunits.units -= 1;
        id = productunits.id
        this.cart.productQty -= 1
        let total = 1 * productunits.price
        this.cart.subTotal = this.cart.subTotal - total;
        this.cart.grandTotal = this.cart.subTotal + this.cart.deliveryCharge;

      }
      else {
        this.removeProduct(productID, id);
      }

    }

  }

  async removeProduct(productID, index, productUnit?) {
    const alert = await this.alertController.create({
      header: 'Remove From Cart!',
      message: 'Are you Sure you want to remove this Product',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            console.log('Confirm Cancel:' + cancel);
          }
        }, {
          text: 'Remove',
          handler: () => {
            console.log('Confirm Okay');
            this.cart.productQty = this.cart.productQty - productUnit || 0;
            let productunits = this.cart.addCart.find(el => el.id === productID);
            let total = 1 * productunits.price;
            this.cart.subTotal = this.cart.subTotal - total;
            this.cart.grandTotal = this.cart.subTotal + this.cart.deliveryCharge;
            productunits.units = 0;
            if (this.cart.productQty == 0) {
              this.cart.subTotal = 0;
              this.cart.grandTotal = 0;
            }
            this.cart.addCart.splice(index, 1)
            this.addCart();
          }
        }
      ]
    });

    await alert.present();
  }

  deliveryAddress() {
    this.route.navigate(['delivery']);
  }
}
