/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-view-category-product',
  templateUrl: './view-category-product.page.html',
  styleUrls: ['./view-category-product.page.scss'],
})
export class ViewCategoryProductPage implements OnInit {
  public categoryProducts: Array<any>;
  public categoryName: string;
  constructor(
    private activeRoute: ActivatedRoute,
    public cart:CartService,
    public dataServ:DataService,
    private route:Router
    ) {


    this.activeRoute.params.subscribe((params: any) => {
      this.categoryName = params.categoryName
      this.categoryProducts = JSON.parse(params.product);
      console.log('product', JSON.parse(params.product))
    })
  }

  ngOnInit() {
  }

  addToCart(index) {
    console.log('product Added', index,)
    this.categoryProducts[index].units = 1
    this.cart.addCart.push(this.categoryProducts[index])
    this.cart.productQty += 1
  }

  updateCart(productID, type) {
    let productunits = this.cart.addCart.find(el => el.id === productID);
    console.log('productunits', productunits)
    if (type == 'add') {
      productunits.units += 1;
      this.cart.productQty += 1
    }
    if (type == 'remove') {
      productunits.units -= 1;
      this.cart.productQty -= 1
    }

  }
  cartPage() {
    this.route.navigate(['cart'])
  }

  viewProduct(product){
    console.log('Product Details',product)
    this.route.navigate(['view-product',{product:JSON.stringify(product)}]);
  }

}
