/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { CartService } from '../services/cart.service';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ModalController, IonSlides } from '@ionic/angular';
import { SearchPage } from '../search/search.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('ionSlides') ionSlides: IonSlides
  public locationAddress;
  public prodoctSlides: Array<any>;
  public categoryItems;

  slideOpts = {
    effect: 'flip'
  };

  public productList: Array<any>;
  constructor(
    public dataServ: DataService,
    public cart: CartService,
    public http: HttpClient,
    private route: Router,
    private modalController: ModalController,
  ) {
    let localData = http.get("assets/information.json").pipe(map((res: any) => res.items))
    localData.subscribe(data => {
      this.categoryItems = data;
    })
    this.locationAddress = ' D-Block,Malviya Nagar,jaipur ';
    this.prodoctSlides = ['assets/imgs/b1.jpg', 'assets/imgs/b2.jpg', 'assets/imgs/b3.png', 'assets/imgs/b4.jpg'];


  }
  ngOnInit() {
    this.ionSlides.startAutoplay();
  }
  slidesDidLoad(slides) {
    this.ionSlides.startAutoplay();
  }

  seeAllProduct() {
    this.route.navigate(['product-list']);
  }
  addLocation() {
    this.route.navigate(['location']);
  }

  goToProducts(title, product) {
    this.route.navigate(['view-category-product', { categoryName: title.name, product: JSON.stringify(product) }]);

  }
  async searchPage() {
    let modal = await this.modalController.create({
      component: SearchPage,
    });
    return await modal.present();
  }

  cartPage() {
    this.route.navigate(['cart'])
  }
}




