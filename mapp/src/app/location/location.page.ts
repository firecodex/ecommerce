/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder,NativeGeocoderOptions,NativeGeocoderResult} from '@ionic-native/native-geocoder/ngx'

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
  items=["New Delhi", "Gurgaon","Jaipur","Goa","Mumbai","Bengaluru","Hyderabad","Kolkata","Pune","Chennai","Chandigarh"];
  location:any
  options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  constructor(
    private route:Router,
    public modalCtrl:ModalController,  public toastCtrl: ToastController, public geolocation:Geolocation, private nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {
  }
  home(){
    this.route.navigate(['home']);
  }

  
  goLocation(){
   this.geolocation.getCurrentPosition().then((res) => {
      
      this.location = res.coords;
      
      this.nativeGeocoder.reverseGeocode(this.location.latitude, this.location.longitude, this.options)
    .then((result: NativeGeocoderResult[]) => alert(JSON.stringify(result[0].countryCode)))
    .catch((error: any) => console.log(error));

    }).catch((error) => {
    // console.log('Error getting location', error);
    });
  }
}
