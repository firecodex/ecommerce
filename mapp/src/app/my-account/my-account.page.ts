/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {
  selectSegment;
  userDetails;
  constructor(
    public cart: CartService,
    private activeRoute :ActivatedRoute,
    private route:Router
  ) {
    this.userDetails = { profileUrl: 'assets/images/user.jpeg', name: 'John Doe', phoneNo: '+91-000-0000-0000', location: 'jaipur' };
    this.activeRoute.params.subscribe((res) =>{
      this.selectSegment  = res.title
      console.log(res.title)
    })
   }

  ngOnInit() {
  }

  editProfile(){
    this.selectSegment = 'profile';
  }
  cartPage() {
    this.route.navigate(['cart'])
  }

}
