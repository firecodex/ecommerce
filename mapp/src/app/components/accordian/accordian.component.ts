/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit, ViewChild, Input,Renderer } from '@angular/core';

@Component({
  selector: 'std-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.scss'],
})
export class AccordianComponent implements OnInit {

 public accordianExpandable = false;
 public cardContentToggle:boolean = false;

  @ViewChild("cardContent") cardContent:any;
  @Input() title:string;
  @Input() image:string;
  @Input() label:string;
  @Input() desc:string;
  @Input() youInm :string;

  constructor(
    public render:Renderer
    ) { }

  ngOnInit() {
    console.log(this.title,this.youInm)
  }

  toggleAccordian(){
    window.scroll({
      behavior:'smooth'
    })
    this.cardContentToggle = !this.cardContentToggle;
    this.accordianExpandable=!this.accordianExpandable;
    if(this.accordianExpandable){
      
    }
  }

  condition()
  {
    return this.accordianExpandable;
  }

}
