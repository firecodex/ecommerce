/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */



import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
})
export class MyProfileComponent implements OnInit {
  public names;
  public email;
  public phoneNumber;
  public  locality;
  public  radio;
  public gender;
  public toggle:boolean;
  public btnColor;
  
  constructor() {
    this.names ="John Doe";
    this.email ="John@enappd.com";
    this.phoneNumber="+91 9878745214";
    this.gender ="Male"
   }

  ngOnInit() {}

  Continue(){
    console.log('this.Continue',this.names,this.email,this.phoneNumber,this.locality,this.radio,this.gender)
  }

}
