/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'popular-deals',
  templateUrl: './popular-deals.component.html',
  styleUrls: ['./popular-deals.component.scss'],
})
export class PopularDealsComponent implements OnInit {
 public popularDeals;
  constructor(
    public dataServ:DataService,
    public cart:CartService,
    private route:Router
  ) { 
    this.popularDeals = dataServ.popularDeals[0]
   }

  ngOnInit() {}

  

  seeAllProduct() {
    console.log('See All Product');
    this.route.navigate(['product-list']);
  }

  addToCart(index) {
    console.log('product Added', index,)
    this.popularDeals.products[index].units = 1
    this.cart.addCart.push(this.popularDeals.products[index])
    this.cart.productQty += 1
  }

  updateCart(productID,type){
    let productunits = this.cart.addCart.find(el => el.id === productID);
    console.log('productunits',productunits)
    if(type == 'add'){
      productunits.units +=1;
      this.cart.productQty += 1
    }
    if(type == 'remove'){
      productunits.units -=1;
      this.cart.productQty -= 1
    }
    
  }

  viewProduct(product){
    console.log('Product Details',product)
    this.route.navigate(['view-product',{product:JSON.stringify(product)}]);
  }


}
