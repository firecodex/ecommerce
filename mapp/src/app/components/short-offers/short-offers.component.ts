/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'short-offers',
  templateUrl: './short-offers.component.html',
  styleUrls: ['./short-offers.component.scss'],
})
export class ShortOffersComponent implements OnInit {

  public shortOffers;
  constructor(
    public cart:CartService,
    public dataServ:DataService,
    private route:Router
  ) { 
    this.shortOffers  = dataServ.shortOffers[0];
  }

  ngOnInit() {}
  
  seeAllProduct() {
    console.log('See All Product');
    this.route.navigate(['product-list']);
  }

  addToCart(index) {
    console.log('product Added', index,)
    this.shortOffers.products[index].units = 1
    this.cart.addCart.push(this.shortOffers.products[index])
    this.cart.productQty += 1
  }

  updateCart(productID,type){
    let productunits = this.cart.addCart.find(el => el.id === productID);
    console.log('productunits',productunits)
    if(type == 'add'){
      productunits.units +=1;
      this.cart.productQty += 1
    }
    if(type == 'remove'){
      productunits.units -=1;
      this.cart.productQty -= 1
    }
    
  }

  viewProduct(product){
    console.log('Product Details',product)
    this.route.navigate(['view-product',{product:JSON.stringify(product)}]);
  }

}
