/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */



import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  public obj = document.getElementById('partitioned');
  public inputFocus1: boolean;
  public inputFocus2: boolean;
  public inputFocus3: boolean;
  public inputFocus4: boolean;
  public otpInput1: any;
  public otpInput2: any;
  public otpInput3: any;
  public otpInput4: any;
  public phoneNumber: any;
  public timeLeft;
  public resendMsgToggle:boolean = false;

  constructor(private route: Router, private menuCtrl: MenuController, public actvRoute: ActivatedRoute) {
  this.actvRoute.params.subscribe((params) => {
    console.log('data coming', params);
  
      this.phoneNumber = params.phoneNumber

  })
  this.inputFocus2 = true;
}

ngOnInit() {
  // this.timerFunction();
}

verify() {
  this.route.navigate(['home']);
}

onchange(num) {
  console.log('change', num, typeof (num))
  if (num == 1) {
    this.inputFocus1 = false;
    this.inputFocus2 = true;
  }
  else if (num == 2) {
    this.inputFocus3 = true;
  }
  else if (num == 3) {
    this.inputFocus4 = true;
  }
  else {
    this.inputFocus1 = false;
    this.inputFocus2 = false;
    this.inputFocus3 = false;
    this.inputFocus4 = false;

  }

}

next(el, val) {
  let numberRegex = /^[0-9\s]*$/;
  var regexp = /^\S*$/;
  console.log(el, 'el', 'val', val, this.otpInput1)
  if (val == '1' && numberRegex.test(this.otpInput1) && regexp.test(this.otpInput1)) {
    console.log('seting')
    el.setFocus();
  }
  else if (val == '2' && numberRegex.test(this.otpInput2) && regexp.test(this.otpInput2)) {
    el.setFocus();
  }
  else if (val == '3' && numberRegex.test(this.otpInput3) && regexp.test(this.otpInput3)) {
    el.setFocus();
  }

}
preview(el) {
  console.log(el)
  if (el == 'otp4') {
    el.setFocus();
  }

}

timerFunction(){
  var countDownDate = new Date("Jan 5, 2021 15:5:25").getTime();


var x = setInterval(function() {

 
  var now = new Date().getTime();
    

  var distance = countDownDate - now;
    
console.log('distance',distance)

  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  console.log('minutes',minutes)
    
  // Output the result in an element with id="demo"
  document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);

}

resendMsg(){
  this.resendMsgToggle = true;
  setTimeout(() => this.resendMsgToggle = false,2000);

}

}
