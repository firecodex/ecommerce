/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.page.html',
  styleUrls: ['./add-address.page.scss'],
})
export class AddAddressPage implements OnInit {

  public names;
  public flat;
  public street;
  public locality;
  public radio;
  public dest;
  public toggle: boolean;
  public btnColor;
  public homeTrue: boolean;
  public officeTrue: boolean;
  
  constructor(
    private route: Router,
    private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.toggle;
    this.homeTrue = false;
    this.officeTrue = false;
    this.btnColor = 'undefined'
  }

  addAddress(item) {
    if (item === 'Home') {
      this.btnColor = "primary";
      this.homeTrue = true;
      this.officeTrue = false;
    }
    if (item === 'Office') {
      this.officeTrue = true;
      this.homeTrue = false;
    }

  }

  async Continue() {
    console.log('this.Continue', this.names, this.flat, this.street, this.locality, this.radio, this.dest)
    const toast = await this.toastCtrl.create({
      message: 'Add new Address Successfully',
      duration: 2000,
      position: 'top'
    });
    toast.present();
    this.route.navigate(['my-account', { title: 'MyAddress' }])
  }

}
