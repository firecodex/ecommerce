/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  public categoryItems;
  
  constructor(
    public http: HttpClient,
    private route: Router
  ) {

    let localData = http.get("assets/information.json").pipe(map((res: any) => res.items))
    localData.subscribe(data => {
      this.categoryItems = data;
      console.log('data', data)
    })
  }

  ngOnInit() {
  }

  goToProducts(title, product) {
    this.route.navigate(['view-category-product', { categoryName: title.name, product: JSON.stringify(product) }]);

  }
}
