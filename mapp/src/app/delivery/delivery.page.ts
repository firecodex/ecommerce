/**
 *Grocery Shopping starter  (https://store.enappd.com/product/grocery-shopping-starterionic4-store2door)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */



import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.page.html',
  styleUrls: ['./delivery.page.scss'],
})
export class DeliveryPage implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  public address;
  public Time;
  public Days;
  public CurrentIndex = 0;
  slideOpts = {
    effect: 'flip'
  };
  public SlideIndex: any = 0;

  constructor(private route: Router) {
    this.address = 'D-Block,Malvia Nagar,Jaipur';
    this.Days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
    this.Time = ["6AM - 9AM", "10AM - 1PM", "4PM - 7PM", "8PM - 11PM", "9AM - 4PM"];
    console.log('days', this.Days, 'time', this.Time)
  }

  ngOnInit() {
  }

  addAddress() {
    console.log('addAddress');
    this.route.navigate(['location']);
  }

  slideTap(index) {
    console.log('index', index)
    this.CurrentIndex = index;
    this.SlideIndex = index
    this.slides.slideTo(index, 200)
  }

  slideChanged(Slides) {
    console.log('slides', Slides)
    this.slides.getActiveIndex().then((res) => {
      console.log('res', res)
      this.CurrentIndex = res;
      this.SlideIndex = res
    })

    console.log(this.SlideIndex)
  }

  check() {
    if (this.SlideIndex > 3)
      this.SlideIndex = 3
  }

  paymentPage() {
    this.route.navigate(['payment']);
  }
}
