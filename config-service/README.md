# Config Server

It's a centralized real-time configuration store.

# Getting Started

### Build

> mvn spring-boot:run

### Endpoint

* [Centralized Configuration](https://admin:freeaccess@localhost:8000/config/dev/master)

