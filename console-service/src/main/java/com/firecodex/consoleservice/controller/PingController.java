package com.firecodex.consoleservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/ping")
public class PingController {
	
	@Autowired Environment environment;
	
	@GetMapping("")
	public String ping() {
		return "Pong";
	}
	
	@GetMapping("/property/{key}")
	public String getProperty(@PathVariable String key) {
		return environment.getProperty(key);
	}
}
