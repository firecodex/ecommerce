package com.firecodex.consoleservice;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ConsoleServiceApplicationTests {

	@LocalServerPort int port;
	private TestRestTemplate restTemplate;
	private URL baseUrl;
	
	@Before
    public void setUp() throws MalformedURLException {
        restTemplate = new TestRestTemplate("user", "password");
        baseUrl = new URL("http://localhost:" + port);
    }

	@Test
	public void whenPingThenPong() throws IllegalStateException, IOException{
		ResponseEntity<String> response = restTemplate.getForEntity(baseUrl.toString().concat("/api/ping"), String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().contains("Pong"));
	}

}
