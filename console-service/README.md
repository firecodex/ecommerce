# Console Server

It's an admin console for the whole application.

# Getting Started

### Build

> mvn spring-boot:run

### Endpoint

* [Console](https://admin:admin@localhost:8008/api/ping)